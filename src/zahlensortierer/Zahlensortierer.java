package zahlensortierer;

import java.util.Arrays;

public class Zahlensortierer {

	public static void main(String[] args) {

		int[] nums = new int[3];
		nums = SetRndNumbersToArray(nums);

		WriteOutArray(nums);
		
		System.out.println("Created random numbers");
		
		Arrays.sort(nums);

		WriteOutArray(nums);
	}

	private static int[] SetRndNumbersToArray(int[] nums) {
		for (int i = 0; i < nums.length; i++) {
			nums[i] = (int) (Math.random() * 50 + 1);
		}
		return nums;
	}

	private static void WriteOutArray(int[] nums) {
		for (int i = 0; i < nums.length; i++) {
			System.out.println(nums[i]);
		}
	}
}
